import { BadRequestException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CategoriasService } from 'src/categorias/categorias.service';
import { CriarDesafioDto } from './dtos/criar-desafio.dto';
import { Desafio, Partida } from './interfaces/desafio.interface';
import { DesafioStatus } from './interfaces/desafio-status.enum';
import { JogadoresService } from 'src/jogadores/jogadores.service';
import { AtualizarDesafioDto } from './dtos/atualizar-desafio.dto';
import { AtribuirDesafioPartidaDto } from './dtos/atribuir-desafio-partida.dto';

@Injectable()
export class DesafiosService {

    constructor(
        @InjectModel('Desafio') private readonly desafioModel: Model<Desafio>,
        @InjectModel('Partida') private readonly partidaModel: Model<Partida>,
        private readonly categoriasService: CategoriasService,
        private readonly jogadoresService: JogadoresService) {}

    async criarDesafio(criarDesafioDto: CriarDesafioDto): Promise<Desafio> {

        const jogadores = await this.jogadoresService.consultarTodosJogadores()

        criarDesafioDto.jogadores.map(jogadorDto => {

            const jogadorFilter = jogadores.filter(jogador => jogador._id == jogadorDto._id)

            if (jogadorFilter.length == 0) {
                throw new BadRequestException(`O id ${jogadorDto._id} não é um jogador!`)
            }

        })

        const solicitanteJogadorPartida = criarDesafioDto.jogadores.filter(jogador => jogador._id == criarDesafioDto.solicitante)
        
        if (solicitanteJogadorPartida.length == 0) {
            throw new BadRequestException(`O solicitante deve ser um jogador da partida!`)
        }

        const categoriaJogador = await this.categoriasService.consultarCategoriaJogador(criarDesafioDto.solicitante)

        if (!categoriaJogador) {
            throw new BadRequestException(`O solicitante precisa estar registrado em uma categoria!`)
        }

        const desafioCriado = new this.desafioModel(criarDesafioDto)
        desafioCriado.categoria = categoriaJogador.categoria
        desafioCriado.dataHoraSolicitacao = new Date()
        desafioCriado.status = DesafioStatus.PENDENTE

        return await desafioCriado.save()

    }

    async consultarTodosDesafios(): Promise<Array<Desafio>> {

        return await this.desafioModel.find()
        .populate("solicitante")
        .populate("jogadores")
        .populate("partida")
        .exec()

    }

    async consultarDesafiosDeUmJogador(_id: any): Promise<Array<Desafio>> {

        const jogadores = await this.jogadoresService.consultarTodosJogadores()

        const jogadorFilter = jogadores.filter(jogador => jogador._id == _id)

        if (jogadorFilter.length == 0) {
            throw new BadRequestException(`O id ${_id} não é um jogador!`)
        }

        return await this.desafioModel.find()
        .where('jogadores')
        .in(_id)
        .populate("solicitante")
        .populate("jogadores")
        .populate("partida")
        .exec()

    }

    async atualizarDesafio(_id: string, atualizarDesafioDto: AtualizarDesafioDto): Promise<void> {

        const desafioEncontrado = await this.desafioModel.findById(_id).exec()

        if (!desafioEncontrado) {
            throw new NotFoundException(`Desafio ${_id} não cadastrado!`)
        }

        if (atualizarDesafioDto.status) {
            desafioEncontrado.dataHoraResposta = new Date()
        }

        desafioEncontrado.status = atualizarDesafioDto.status
        desafioEncontrado.dataHoraDesafio = atualizarDesafioDto.dataHoraDesafio

        await this.desafioModel.findOneAndUpdate({_id},{$set: desafioEncontrado}).exec()

    }

    async excluirDesafio(_id: string): Promise<void> {

        const desafioEncontrado = await this.desafioModel.findById(_id).exec()

        if (!desafioEncontrado) {
            throw new BadRequestException(`Desafio ${_id} não cadastrado!`)
        }

        desafioEncontrado.status = DesafioStatus.CANCELADO

        await this.desafioModel.findOneAndUpdate({_id},{$set: desafioEncontrado}).exec()

    }

    async atribuirDesafioPartida(_id: string, atribuirDesafioPartidaDto: AtribuirDesafioPartidaDto): Promise<void> {

        const desafioEncontrado = await this.desafioModel.findById(_id).exec()

        if (!desafioEncontrado) {
            throw new BadRequestException(`Desafio ${_id} não cadastrado!`)
        }

        const jogadorVencedor = atribuirDesafioPartidaDto.def
        const jogadorFilter = desafioEncontrado.jogadores.filter(jogador => jogador._id == jogadorVencedor)
        
        if (jogadorFilter.length == 0) {
            throw new BadRequestException(`O vencedor ${jogadorVencedor} não faz parte do desafio!`)
        }

        const partidaCriada = new this.partidaModel(atribuirDesafioPartidaDto)

        partidaCriada.categoria = desafioEncontrado.categoria
        partidaCriada.jogadores = desafioEncontrado.jogadores

        const resultado = await partidaCriada.save()

        desafioEncontrado.status = DesafioStatus.REALIZADO
        desafioEncontrado.partida = resultado._id

        try {

            await this.desafioModel.findOneAndUpdate({_id},{$set: desafioEncontrado}).exec()

        } catch (error) {

            await this.partidaModel.deleteOne({_id: resultado._id}).exec()
            throw new InternalServerErrorException()

        }

    }

}
